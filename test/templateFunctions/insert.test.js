const knex = require('knex');
const insert = require('../../src/templateFunctions/insert');

const appInstanceOfExpress = {};
let insertFunction = null;
beforeAll(async () => {
  appInstanceOfExpress.db = await knex({
    client: 'mysql',
    connection: {
      host: 'localhost',
      user: 'root',
      // password: 'rootpasswd',
      database: 'skeleton',
    },
  });

  insertFunction = await insert(appInstanceOfExpress, 'games');

  await appInstanceOfExpress.db.schema.dropTableIfExists('games');
  await appInstanceOfExpress.db.schema.createTable('games', (table) => {
    table.increments();
    table.string('name');
    table.integer('releaseDate');
  });
});

test('should insert a game in the table games', async () => {
  const game = { name: 'Crysis', releaseDate: 23052020 };
  [game.id] = await insertFunction(game);
  const [gameOnDatabase] = await appInstanceOfExpress.db('games').where({ id: game.id }).select();
  expect(game).toEqual(gameOnDatabase);
});
