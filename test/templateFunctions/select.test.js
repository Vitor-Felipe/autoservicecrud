const knex = require('knex');
const select = require('../../src/templateFunctions/select');

const appInstanceOfExpress = {};
const insertedUsers = [];
let selectFunction = null;
beforeAll(async () => {
  appInstanceOfExpress.db = await knex({
    client: 'mysql',
    connection: {
      host: 'localhost',
      user: 'root',
      // password: 'rootpasswd',
      database: 'skeleton',
    },
  });

  selectFunction = select(appInstanceOfExpress, 'users');
  await appInstanceOfExpress.db.schema.dropTableIfExists('users');
  await appInstanceOfExpress.db.schema.createTable('users', (table) => {
    table.increments();
    table.string('name');
    table.string('passwd');
    table.string('email');
  });

  insertedUsers[0] = {
    name: `test-${Date.now()}`,
    passwd: `${Date.now()}`,
    email: `${Date.now()}@.com`,
  };
  insertedUsers[1] = {
    name: `test2-${Date.now()}`,
    passwd: `${Date.now()}`,
    email: `${Date.now()}@.com`,
  };

  [insertedUsers[0].id] = await appInstanceOfExpress.db('users').insert(insertedUsers[0], 'id');
  [insertedUsers[1].id] = await appInstanceOfExpress.db('users').insert(insertedUsers[1], 'id');
});

test('should select users from the table users', async () => {
  const users = await selectFunction();
  expect(users.length).toBeGreaterThan(1);
  expect(users[0]).toHaveProperty('id');
  expect(users[0]).toHaveProperty('name');
  expect(users[0]).toHaveProperty('passwd');
  expect(users[0]).toHaveProperty('email');
});

test('should select a user with name defined on beforeAll', async () => {
  const users = await selectFunction({ name: insertedUsers[0].name });
  expect(users[0]).toEqual(insertedUsers[0]);
});
