const knex = require('knex');
const update = require('../../src/templateFunctions/update');

const appInstanceOfExpress = {};
const insertedVirus = [];
let updateFunction = null;
beforeAll(async () => {
  appInstanceOfExpress.db = await knex({
    client: 'mysql',
    connection: {
      host: 'localhost',
      user: 'root',
      // password: 'rootpasswd',
      database: 'skeleton',
    },
  });

  updateFunction = update(appInstanceOfExpress, 'human_virus');
  await appInstanceOfExpress.db.schema.dropTableIfExists('human_virus');
  await appInstanceOfExpress.db.schema.createTable('human_virus', (table) => {
    table.increments();
    table.string('name');
    table.string('transmission');
  });

  insertedVirus[0] = {
    name: 'Human coronavirus',
    transmission: 'Respiratory',
  };
  insertedVirus[1] = {
    name: 'Zika virus',
    transmission: 'arthropod bite',
  };
  insertedVirus[2] = {
    name: 'Yellow fever virus',
    transmission: 'arthropod bite',
  };

  insertedVirus[0].id = await appInstanceOfExpress.db('human_virus').insert(insertedVirus[0], 'id');
  insertedVirus[1].id = await appInstanceOfExpress.db('human_virus').insert(insertedVirus[1], 'id');
  insertedVirus[2].id = await appInstanceOfExpress.db('human_virus').insert(insertedVirus[2], 'id');
});

test('should update by id the name of the virus', async () => {
  const newVirusName = `${insertedVirus[0].name} Updated`;
  const coronaVirusIsUpdated = await updateFunction(
    { id: insertedVirus[0].id }, { name: newVirusName },
  );
  expect(coronaVirusIsUpdated).toBe(1);
  const [coronaVirusUpdatedOnDatabase] = await appInstanceOfExpress.db('human_virus').where({ id: insertedVirus[0].id }).select();
  expect(coronaVirusUpdatedOnDatabase).not.toEqual(insertedVirus[0]);
  expect(coronaVirusUpdatedOnDatabase.name).toBe(newVirusName);
});


test('should update all virus with the transmission type equal to arthropod bite', async () => {
  const newName = 'Updated because i have a transmission by arthropod bite';
  const coronaVirusIsUpdated = await updateFunction({ transmission: 'arthropod bite' }, { name: newName });
  expect(coronaVirusIsUpdated).toBe(1);
  const coronaVirusesUpdatedOnDatabase = await appInstanceOfExpress.db('human_virus').where({ id: insertedVirus[0].id }).select();
  for (let i = coronaVirusesUpdatedOnDatabase.length - 1; i >= 0; i -= 1) {
    const coronaVirusUpdatedOnDatabase = coronaVirusesUpdatedOnDatabase[i];
    expect(coronaVirusUpdatedOnDatabase.name).toBe(newName);
  }
});
