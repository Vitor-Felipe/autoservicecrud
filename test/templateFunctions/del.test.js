const knex = require('knex');
const del = require('../../src/templateFunctions/del');

const appInstanceOfExpress = {};
let delFunction = null;

beforeAll(async () => {
  appInstanceOfExpress.db = await knex({
    client: 'mysql',
    connection: {
      host: 'localhost',
      user: 'root',
      // password: 'rootpasswd',
      database: 'skeleton',
    },
  });

  delFunction = del(appInstanceOfExpress, 'virus');
  await appInstanceOfExpress.db.schema.dropTableIfExists('virus');
  await appInstanceOfExpress.db.schema.createTable('virus', (table) => {
    table.increments();
    table.string('name');
  });
});

test('should delete a virus by id in the table virus', async () => {
  const virus = { name: 'Corona' };
  virus.id = await appInstanceOfExpress.db('virus').insert(virus, 'id');
  await delFunction({ id: virus.id });
  const virusOnDatabase = await appInstanceOfExpress.db('virus').where({ id: virus.id }).select();
  expect(virusOnDatabase.length).toBe(0);
});
